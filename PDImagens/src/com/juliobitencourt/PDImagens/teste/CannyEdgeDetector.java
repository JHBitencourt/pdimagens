package com.juliobitencourt.PDImagens.teste;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class CannyEdgeDetector {

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        Mat rgbImage = Imgcodecs.imread("imgs/bricks.jpg");

        Mat imageGray = new Mat();
        Imgproc.cvtColor(rgbImage, imageGray, Imgproc.COLOR_BGR2GRAY);

        Mat imageCanny = new Mat();
        Imgproc.Canny(imageGray, imageCanny, 100, 500, 5, true);

        Imgcodecs.imwrite("output_canny.jpg", imageCanny);
    }
}

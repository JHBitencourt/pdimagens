package com.juliobitencourt.PDImagens.controller;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import com.juliobitencourt.PDImagens.controller.helper.AddSubtractCommunicatorHelper;
import com.juliobitencourt.PDImagens.model.business.AddSubtractBusiness;
import com.juliobitencourt.PDImagens.model.business.AddSubtractBusiness.Action;
import com.juliobitencourt.PDImagens.model.entities.RGB;

public class AddSubtractController {

    private AddSubtractBusiness addSubtractBusiness;
    
    private AddSubtractCommunicatorHelper firstHelper;
    private AddSubtractCommunicatorHelper secondHelper;
    
    private BufferedImage newImage;
    private WritableRaster newRaster;

    public BufferedImage execute(Action action, int fator, BufferedImage firstImage, BufferedImage secondImage) {
        firstHelper = new AddSubtractCommunicatorHelper(firstImage, this);
        secondHelper = new AddSubtractCommunicatorHelper(secondImage, this);
        addSubtractBusiness = new AddSubtractBusiness(firstHelper, secondHelper);

        newImage = addSubtractBusiness.getNewImage();
        newRaster = newImage.getRaster();

        addSubtractBusiness.execute(action, fator);

        return newImage;
    }

    public void updatePixel(int x, int y, RGB newRGB) {
        int currentPixelInfos[] = new int[4];
        newRaster.getPixel(x, y, currentPixelInfos);

        currentPixelInfos[0] = newRGB.getRed();
        currentPixelInfos[1] = newRGB.getGreen();
        currentPixelInfos[2] = newRGB.getBlue();
        newRaster.setPixel(x, y, currentPixelInfos);
    }
}
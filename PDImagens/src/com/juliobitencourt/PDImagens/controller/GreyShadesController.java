package com.juliobitencourt.PDImagens.controller;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import com.juliobitencourt.PDImagens.model.business.GreyShadesBusiness;
import com.juliobitencourt.PDImagens.model.business.GreyShadesBusiness.GreyShadesTecnique;
import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

public class GreyShadesController implements PixelCommunicator {

    GreyShadesBusiness greyShadesBusiness = new GreyShadesBusiness(this);

    private BufferedImage image;
    private BufferedImage newImage;
    private WritableRaster raster;
    private int currentPixelInfos[] = new int[4];

    public BufferedImage turnIntoGrey(BufferedImage image, GreyShadesTecnique tecnique) {
        return turnIntoGrey(image, tecnique, null);
    }

    public BufferedImage turnIntoGrey(BufferedImage image, GreyShadesTecnique tecnique, int[] rgbPercents) {
        this.image = image;
        this.raster = image.getRaster();
        this.newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);

        greyShadesBusiness.turnIntoGrey(tecnique, rgbPercents);

        newImage.setData(raster);
        return newImage;
    }

    @Override
    public Pixel getPixel(int x, int y) {
        raster.getPixel(x, y, currentPixelInfos);
        RGB rgb = new RGB(currentPixelInfos[0], currentPixelInfos[1], currentPixelInfos[2]);
        return new Pixel(x, y, rgb);
    }

    @Override
    public void updatePixel(int x, int y, RGB newRGB) {
        currentPixelInfos[0] = newRGB.getRed();
        currentPixelInfos[1] = newRGB.getGreen();
        currentPixelInfos[2] = newRGB.getBlue();
        raster.setPixel(x, y, currentPixelInfos);
    }

    @Override
    public int getImageWidth() {
        return image.getWidth();
    }

    @Override
    public int getImageHeight() {
        return image.getHeight();
    }
}
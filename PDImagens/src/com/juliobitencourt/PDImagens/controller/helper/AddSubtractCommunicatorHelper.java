package com.juliobitencourt.PDImagens.controller.helper;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import com.juliobitencourt.PDImagens.controller.AddSubtractController;
import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

public class AddSubtractCommunicatorHelper implements PixelCommunicator {

    private BufferedImage image;
    private WritableRaster raster;
    private int currentPixelInfos[] = new int[4];

    private AddSubtractController controllerInstance;

    public AddSubtractCommunicatorHelper(BufferedImage image, AddSubtractController instance) {
        this.controllerInstance = instance;
        this.image = image;
        this.raster = image.getRaster();
    }

    @Override
    public Pixel getPixel(int x, int y) {
        try {
            raster.getPixel(x, y, currentPixelInfos);
            RGB rgb = new RGB(currentPixelInfos[0], currentPixelInfos[1], currentPixelInfos[2]);

            return new Pixel(x, y, rgb);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("OutOfBounds");
            return null;
        }
    }

    @Override
    public void updatePixel(int x, int y, RGB newRGB) {
        controllerInstance.updatePixel(x, y, newRGB);
    }

    @Override
    public int getImageWidth() {
        return image.getWidth();
    }

    @Override
    public int getImageHeight() {
        return image.getHeight();
    }
}
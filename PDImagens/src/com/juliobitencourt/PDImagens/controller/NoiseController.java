package com.juliobitencourt.PDImagens.controller;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import com.juliobitencourt.PDImagens.model.business.NoiseBusiness;
import com.juliobitencourt.PDImagens.model.business.NoiseBusiness.NoiseTecnique;
import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelTecnique;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

public class NoiseController implements PixelCommunicator {

    private NoiseBusiness noiseBusiness = new NoiseBusiness(this);

    private BufferedImage image;
    private BufferedImage newImage;
    private WritableRaster raster;
    private int currentPixelInfos[] = new int[4];

    public BufferedImage removeNoise(BufferedImage image, PixelTecnique pixelTecnique, NoiseTecnique noiseTecnique) {
        this.image = image;
        this.raster = image.getRaster();
        this.newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);

        noiseBusiness.removeNoise(pixelTecnique, noiseTecnique);

        newImage.setData(raster);
        return newImage;
    }

    @Override
    public Pixel getPixel(int x, int y) {
        raster.getPixel(x, y, currentPixelInfos);
        RGB rgb = new RGB(currentPixelInfos[0], currentPixelInfos[1], currentPixelInfos[2]);
        return new Pixel(x, y, rgb);
    }

    @Override
    public void updatePixel(int x, int y, RGB newRGB) {
        currentPixelInfos[0] = newRGB.getRed();
        currentPixelInfos[1] = newRGB.getGreen();
        currentPixelInfos[2] = newRGB.getBlue();
        raster.setPixel(x, y, currentPixelInfos);
    }

    @Override
    public int getImageWidth() {
        return image.getWidth();
    }

    @Override
    public int getImageHeight() {
        return image.getHeight();
    }
}
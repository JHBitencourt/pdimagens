package com.juliobitencourt.PDImagens.controller;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import com.juliobitencourt.PDImagens.model.business.DrawingBusiness;
import com.juliobitencourt.PDImagens.model.business.DrawingBusiness.Coordinates;
import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

/**
 * Classe respons�vel por fazer a liga��o da View com o Model
 * 
 * @author Julio Henrique Bitencourt<julio.henrique.b@gmail.com>
 *
 */
public class DrawingController implements PixelCommunicator {

    private DrawingBusiness drawingBusiness = new DrawingBusiness();
    private BufferedImage image;
    private BufferedImage newImage;
    private WritableRaster raster;
    private int currentPixelInfos[] = new int[4];

    public BufferedImage drawSquare(BufferedImage image, int[] startPositions, int[] endPositions) {
        DrawingBusiness.Coordinates coordinates = fillCoordinates(new DrawingBusiness.Coordinates(), startPositions, endPositions);

        return drawingBusiness.drawSquare(image, coordinates);
    }

    /**
     * Preenche as coordenadas de inicio e t�rmino dos cliques.
     * 
     * @param coordinates
     * @param startPositions
     * @param endPositions
     * @return
     */
    private Coordinates fillCoordinates(Coordinates coordinates, int[] startPositions, int[] endPositions) {
        //Inverte as posi��es caso necess�rio, para ser poss�vel desenhar a figura a partir dos quatro cantos.
        for (int i = 0; i < endPositions.length; i++) {
            if(endPositions[i] < startPositions[i]) {
                int position = startPositions[i];
                startPositions[i] = endPositions[i];
                endPositions[i] = position;
            }
        }

        coordinates.setxStart(startPositions[0]);
        coordinates.setyStart(startPositions[1]);
        coordinates.setxFinish(endPositions[0]);
        coordinates.setyFinish(endPositions[1]);

        return coordinates;
    }

    public BufferedImage drawGreySquare(BufferedImage image, int[] startPositions, int[] endPositions) {
        this.image = image;
        this.raster = image.getRaster();
        this.newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);

        drawingBusiness.drawGreySquare(this, fillCoordinates(new DrawingBusiness.Coordinates(), startPositions, endPositions));

        newImage.setData(raster);
        return newImage;
    }

    @Override
    public Pixel getPixel(int x, int y) {
        raster.getPixel(x, y, currentPixelInfos);
        RGB rgb = new RGB(currentPixelInfos[0], currentPixelInfos[1], currentPixelInfos[2]);
        return new Pixel(x, y, rgb);
    }

    @Override
    public void updatePixel(int x, int y, RGB newRGB) {
        currentPixelInfos[0] = newRGB.getRed();
        currentPixelInfos[1] = newRGB.getGreen();
        currentPixelInfos[2] = newRGB.getBlue();
        raster.setPixel(x, y, currentPixelInfos);
    }

    @Override
    public int getImageWidth() {
        return image.getWidth();
    }

    @Override
    public int getImageHeight() {
        return image.getHeight();
    }
}
package com.juliobitencourt.PDImagens.model.business;

import java.util.Arrays;
import java.util.List;

import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelTecnique;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

/**
 * Classe contendo a l�gica para remo��o de ru�dos de uma determinada imagem.
 * 
 * Esta classe � independente de plataforma(web, desktop ou mobile), por conta
 * disso ao tentar utiliz�-la � necess�rio implementar o seu Communicator.   
 * 
 * @author Julio Henrique Bitencourt(julio.henrique.b@gmail.com)
 */
public class NoiseBusiness {

    private PixelCommunicator communicator;
    private PixelBusiness pixelBusiness;
    private PixelTecnique pixelTecnique;
    private NoiseTecnique noiseTecnique;

    public NoiseBusiness(PixelCommunicator communicator) {
        this.communicator = communicator;
        this.pixelBusiness = new PixelBusiness(communicator);
    }

    public void removeNoise(PixelTecnique pixelTecnique, NoiseTecnique noiseTecnique) {
        this.pixelTecnique = pixelTecnique;
        this.noiseTecnique = noiseTecnique;

        Pixel currentPixel = null;
        RGB newRGB = null;

        for (int row = 1; row < communicator.getImageWidth() - 1; row++) {
            for (int column = 1; column < communicator.getImageHeight() - 1; column++) {
                currentPixel = communicator.getPixel(row, column);
                newRGB = getNewPixelRGB(currentPixel);
                communicator.updatePixel(currentPixel.getX(), currentPixel.getY(), newRGB);
            }
        }
    }

    private RGB getNewPixelRGB(Pixel pixel) {
        int[] valuesR = new int[pixelTecnique.getPixelsNumber()];
        int[] valuesG = new int[pixelTecnique.getPixelsNumber()];
        int[] valuesB = new int[pixelTecnique.getPixelsNumber()];

        List<Pixel> pixels =  pixelBusiness.getNeighborPixels(pixel.getX(), pixel.getY(), pixelTecnique);

        int index = 0;
        for (Pixel p : pixels) {
            valuesR[index] = p.getRgb().getRed();
            valuesG[index] = p.getRgb().getGreen();
            valuesB[index] = p.getRgb().getBlue();
            index++;
        }

        orderArrays(valuesR, valuesG, valuesB);

        RGB rgb = new RGB();
        rgb.setRed(calculateRGBValue(valuesR));
        rgb.setGreen(calculateRGBValue(valuesG));
        rgb.setBlue(calculateRGBValue(valuesB));

        return rgb;
    }

    private int calculateRGBValue(int[] values) {
        switch (noiseTecnique) {
        case AVERAGE:
            return calculateAverage(values);
        case MEDIAN:
            return calculateMedian(values);
        }

        return 0;
    }

    /**
     * M�todo que calcular� a mediana de um vetor de inteiros.
     * M�dia � igual a soma de todos os valores dividido pela
     * quantidade de valores.
     * @param values
     * @return
     */
    private int calculateAverage(int[] values) {
        int sum = 0;
        for (int value : values) {
            sum += value;
        }

        return new Double(Math.floor(sum / values.length)).intValue();
    }

    /**
     * M�todo que calcular� a mediana de um vetor de inteiros.
     * Se o vetor possui um n�mero par de valores, a mediana corresponde a
     * divis�o dos 2 valores do meio. 
     * Exemplo [10, 20, 30, 40] > Mediana = (20 + 30) / 2 = 25
     * @param values Vetor ordenado com valores
     * @return
     */
    private int calculateMedian(int[] values) {
        int remainder = values.length % 2;

        int divisionResult = values.length/2;
        if(remainder == 1) {
            return values[new Double(Math.floor(divisionResult)).intValue()];
        } else {
            int middleOne = divisionResult - 1;
            int middleTwo = divisionResult;
            int sum = values[middleOne] + values[middleTwo];
            return new Double(Math.floor(sum / 2)).intValue();
        }
    }

    private void orderArrays(int[]... arrays) {
        for (int[] array : arrays) {
            Arrays.sort(array);
        }
    }

    public enum NoiseTecnique {
        AVERAGE,
        MEDIAN;
    }
}
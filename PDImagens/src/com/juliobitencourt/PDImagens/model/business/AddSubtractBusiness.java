package com.juliobitencourt.PDImagens.model.business;

import java.awt.image.BufferedImage;

import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

public class AddSubtractBusiness {

    private PixelCommunicator firstCommunicator;
    private PixelCommunicator secondCommunicator;

    private int newImageWidht;
    private int newImageHeight;

    public AddSubtractBusiness(PixelCommunicator firstCommunicator, PixelCommunicator secondCommunicator) {
        this.firstCommunicator = firstCommunicator;
        this.secondCommunicator = secondCommunicator;
    }

    public void execute(Action action, int fator) {
        Pixel currentPixelOne = null;
        Pixel currentPixelTwo = null;

        RGB newRGB = null;

        for (int row = 1; row < newImageWidht - 1; row++) {
            for (int column = 1; column < newImageHeight - 1; column++) {
                currentPixelOne = firstCommunicator.getPixel(row, column);
                currentPixelTwo = secondCommunicator.getPixel(row, column);

                newRGB = getNewPixelRGB(currentPixelOne, currentPixelTwo, action, fator);
                firstCommunicator.updatePixel(row, column, newRGB);
            }
        }
    }

    private RGB getNewPixelRGB(Pixel currentPixelOne, Pixel currentPixelTwo, Action action, int fator) {
        if(currentPixelOne == null && currentPixelTwo == null) {
            RGB rgb = new RGB();
            rgb.setRed(0);
            rgb.setGreen(0);
            rgb.setBlue(0);
            return rgb;
        }

        if(currentPixelOne == null) {
            return getRgbPixel(currentPixelTwo);
        }

        if(currentPixelTwo == null) {
            return getRgbPixel(currentPixelOne);
        }

        RGB rgb = new RGB();
        rgb.setRed(calculateRGBValue(currentPixelOne.getRgb().getRed(), currentPixelTwo.getRgb().getRed(), action, fator));
        rgb.setGreen(calculateRGBValue(currentPixelOne.getRgb().getGreen(), currentPixelTwo.getRgb().getGreen(), action,fator));
        rgb.setBlue(calculateRGBValue(currentPixelOne.getRgb().getBlue(), currentPixelTwo.getRgb().getBlue(), action,fator));

        return rgb;
    }

    private RGB getRgbPixel(Pixel pixel) {
        RGB rgb = new RGB();
        rgb.setRed(pixel.getRgb().getRed());
        rgb.setGreen(pixel.getRgb().getGreen());
        rgb.setBlue(pixel.getRgb().getBlue());
        return rgb;
    }

    private int calculateRGBValue(int valueOne, int valueTwo, Action action, int fator) {
        double fatorOne = (double)fator / 100;
        double fatorTwo = (double)(100 - fator) / 100;
        int newValue = 0;

        switch (action) {
        case ADD:
            newValue = (int) ((valueOne * fatorOne) + (valueTwo * fatorTwo));
            break;
        case SUBTRACT:
            newValue = (int) ((valueOne * fatorOne) - (valueTwo * fatorTwo));
            break;
        }

        return newValue < 0 ? 0 : newValue;
    }

    public BufferedImage getNewImage() {
        int firstWidth = firstCommunicator.getImageWidth();
        int secondWidth = secondCommunicator.getImageWidth();
        newImageWidht = firstWidth >= secondWidth ? firstWidth : secondWidth;

        int firstHeight = firstCommunicator.getImageHeight();
        int secondHeight = secondCommunicator.getImageHeight();
        newImageHeight = firstHeight >= secondHeight ? firstHeight : secondHeight;

        return new BufferedImage(newImageWidht, newImageHeight, BufferedImage.TYPE_INT_RGB);
    }

    public enum Action {
        ADD, SUBTRACT
    }
}
package com.juliobitencourt.PDImagens.model.business;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

/**
 * Classe respons�vel por aplicar as regras de Desenho em Imagens.
 * 
 * @author Julio Henrique Bitencourt<julio.henrique.b@gmail.com>
 */
public class DrawingBusiness {

    private PixelCommunicator communicator;

    private static final int STROKE_SIZE = 8;

    /**
     * M�todo respons�vel por desenhar um quadrado na imagem.
     * 
     * @param image
     * @param coordinates
     * @return
     */
    public BufferedImage drawSquare(BufferedImage image, Coordinates coordinates) {
        int[] xPositions = new int[]{coordinates.xStart, coordinates.xStart, coordinates.xFinish, coordinates.xFinish};
        int[] yPositions = new int[]{coordinates.yStart, coordinates.yFinish, coordinates.yFinish, coordinates.yStart};

        Graphics graph = image.getGraphics();
        Graphics2D graph2d = (Graphics2D) graph.create();
        graph2d.setStroke(new BasicStroke(STROKE_SIZE));
        graph2d.setColor(Color.BLACK);
        graph2d.drawPolygon(xPositions, yPositions, xPositions.length);
        graph2d.dispose();
        return image;
    }

    /**
     * M�todo respons�vel por desenhar um quadrado e preench�-lo com tons de cinza.
     * @param pixelCommunicator
     * @param coordinates
     */
    public void drawGreySquare(PixelCommunicator pixelCommunicator, Coordinates coordinates) {
        communicator = pixelCommunicator;

        Pixel currentPixel = null;
        RGB newRGB = null;

        for (int row = coordinates.xStart; row <= coordinates.xFinish; row++) {
            for (int column = coordinates.yStart; column <= coordinates.yFinish; column++) {
                currentPixel = communicator.getPixel(row, column);
                newRGB = getNewPixelRGB(row, column, currentPixel, coordinates);
                communicator.updatePixel(row, column, newRGB);
            }
        }
    }

    /**
     * M�todo respons�vel por pintar o pixel em cinza.
     * 
     * @param x
     * @param y
     * @param currentPixel
     * @param coordinates
     * @return
     */
    private RGB getNewPixelRGB(int x, int y, Pixel currentPixel, Coordinates coordinates) {
        if((y > coordinates.yStart && y < coordinates.yFinish) && (x > coordinates.xStart && x < coordinates.xFinish)) {
            return getGreyPixel(currentPixel);
        }

        return currentPixel.getRgb();
    }

    /**
     * Retorna o pixel em tons de cinza.
     * 
     * @param currentPixel
     * @return
     */
    private RGB getGreyPixel(Pixel currentPixel) {
        int red = currentPixel.getRgb().getRed();
        int green = currentPixel.getRgb().getGreen();
        int blue = currentPixel.getRgb().getBlue();

        int newRGBValue = (red + green + blue) / 3;

        RGB rgb = new RGB();
        rgb.setRed(newRGBValue);
        rgb.setGreen(newRGBValue);
        rgb.setBlue(newRGBValue);
        return rgb;
    }

    /**
     * Inner Class que representa as coordenadas selecionadas na imagem.
     * @author Julio Henrique Bitencourt<julio.henrique.b@gmail.com>
     *
     */
    public static class Coordinates {
        int xStart;
        int xFinish;
        int yStart;
        int yFinish;

        public void setxStart(int xStart) {
            this.xStart = xStart;
        }

        public void setxFinish(int xFinish) {
            this.xFinish = xFinish;
        }

        public void setyStart(int yStart) {
            this.yStart = yStart;
        }

        public void setyFinish(int yFinish) {
            this.yFinish = yFinish;
        }
    }
}
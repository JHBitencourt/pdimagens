package com.juliobitencourt.PDImagens.model.business;

import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

public class BlackWhiteBusiness {

    public static final int BLACK = 0;
    public static final int WHITE = 255;

    private PixelCommunicator communicator;

    public BlackWhiteBusiness(PixelCommunicator communicator) {
        this.communicator = communicator;
    }

    public void applyLimiarizacaoFilter(int currentFilterValue) {
        Pixel currentPixel = null;
        RGB newRGB = null;

        for (int row = 1; row < communicator.getImageWidth() - 1; row++) {
            for (int column = 1; column < communicator.getImageHeight() - 1; column++) {
                currentPixel = communicator.getPixel(row, column);
                newRGB = getNewPixelRGB(currentPixel, currentFilterValue);
                communicator.updatePixel(row, column, newRGB);
            }
        }
    }

    private RGB getNewPixelRGB(Pixel currentPixel, int currentFilterValue) {
        int red = currentPixel.getRgb().getRed();
        int green = currentPixel.getRgb().getGreen();
        int blue = currentPixel.getRgb().getBlue();

        RGB rgb = new RGB();
        rgb.setRed(red <= currentFilterValue ? BLACK : WHITE);
        rgb.setGreen(green <= currentFilterValue ? BLACK : WHITE);
        rgb.setBlue(blue <= currentFilterValue ? BLACK : WHITE);
        return rgb;
    }
}
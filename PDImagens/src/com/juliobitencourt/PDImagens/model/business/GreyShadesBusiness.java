package com.juliobitencourt.PDImagens.model.business;

import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

public class GreyShadesBusiness {

    private PixelCommunicator communicator;
    private GreyShadesTecnique tecnique;

    public GreyShadesBusiness(PixelCommunicator communicator) {
        this.communicator = communicator;
    }

    public void turnIntoGrey(GreyShadesTecnique tecnique, int[] rgbPercents) {
        this.tecnique = tecnique;

        Pixel currentPixel = null;
        RGB newRGB = null;

        for (int row = 1; row < communicator.getImageWidth() - 1; row++) {
            for (int column = 1; column < communicator.getImageHeight() - 1; column++) {
                currentPixel = communicator.getPixel(row, column);
                newRGB = getNewPixelRGB(currentPixel, rgbPercents);
                communicator.updatePixel(row, column, newRGB);
            }
        }
    }

    private RGB getNewPixelRGB(Pixel currentPixel, int[] rgbPercents) {
        int newRGBValue = 0;

        int red = currentPixel.getRgb().getRed();
        int green = currentPixel.getRgb().getGreen();
        int blue = currentPixel.getRgb().getBlue();
        switch (tecnique) {
        case SIMPLES:
            newRGBValue = calculateRGBSimples(red, green, blue);
            break;
        case PONDERADA:
            newRGBValue = calculateRGBPonderada(red, green, blue, rgbPercents);
            break;
        }

        RGB rgb = new RGB();
        rgb.setRed(newRGBValue);
        rgb.setGreen(newRGBValue);
        rgb.setBlue(newRGBValue);
        return rgb;
    }

    public int calculateRGBSimples(int red, int green, int blue) {
        return (red + green + blue) / 3;
    }

    private int calculateRGBPonderada(int red, int green, int blue, int[] rgbPercents) {
        red = red * rgbPercents[0];
        green = green * rgbPercents[1];
        blue = blue * rgbPercents[2];

        return (red + green + blue) / 100; 
    }

    public enum GreyShadesTecnique {
        SIMPLES,
        PONDERADA
    }
}
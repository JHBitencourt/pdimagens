package com.juliobitencourt.PDImagens.model.business;

import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

public class NegativaBusiness {

    private PixelCommunicator communicator;

    public static final int PATTERN_NEGATIVA = 255;

    public NegativaBusiness(PixelCommunicator communicator) {
        this.communicator = communicator;
    }

    public void applyNegativa() {
        Pixel currentPixel = null;
        RGB newRGB = null;

        for (int row = 1; row < communicator.getImageWidth() - 1; row++) {
            for (int column = 1; column < communicator.getImageHeight() - 1; column++) {
                currentPixel = communicator.getPixel(row, column);
                newRGB = getNewPixelRGB(currentPixel);
                communicator.updatePixel(row, column, newRGB);
            }
        }
    }

    private RGB getNewPixelRGB(Pixel currentPixel) {
        int red = currentPixel.getRgb().getRed();
        int green = currentPixel.getRgb().getGreen();
        int blue = currentPixel.getRgb().getBlue();

        RGB rgb = new RGB();
        rgb.setRed(PATTERN_NEGATIVA - red);
        rgb.setGreen(PATTERN_NEGATIVA - green);
        rgb.setBlue(PATTERN_NEGATIVA - blue);
        return rgb;
    }

}
package com.juliobitencourt.PDImagens.model.business;

import java.util.ArrayList;
import java.util.Random;

import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.entities.Group;
import com.juliobitencourt.PDImagens.model.entities.Histograma;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

/**
 * Classe contendo a l�gica principal para segmenta��o de uma Imagem.
 * 
 * @author Julio Henrique Bitencourt e Marcus Hert Da Cor�gio
 */
public class SegmentationBusiness {

    private PixelCommunicator communicator;
    private HistogramaBusiness histogramaBusiness;
    private GreyShadesBusiness greyBusiness;
    private ArrayList<Group> groups = new ArrayList<>();

    private Integer minimum;
    private Integer maximum;

    public SegmentationBusiness(PixelCommunicator communicator) {
        this.communicator = communicator;
        this.histogramaBusiness = new HistogramaBusiness(communicator);
    }

    /**
     * M�todo principal da classe, utiliza a implementa��o do PixelCommunicator
     * para recuperar os pixels da Imagem.
     * Desta forma o model fica desacoplado de qualquer implementa��o(desktop, web, mobile).
     * 
     * @param groupNumber
     */
    public void segmentateImage(int groupNumber) {
        Histograma histograma = histogramaBusiness.extractHistograma();
        getMinMaxValues(histograma.getValuesR());
        getMinMaxValues(histograma.getValuesG());
        getMinMaxValues(histograma.getValuesB());

        createGroups(groupNumber);

        Pixel currentPixel = null;
        RGB newRGB = null;

        greyBusiness = new GreyShadesBusiness(communicator);
        for (int row = 0; row < communicator.getImageWidth(); row++) {
            for (int column = 0; column < communicator.getImageHeight(); column++) {
                currentPixel = communicator.getPixel(row, column);
                newRGB = getNewPixelRGB(currentPixel);
                communicator.updatePixel(currentPixel.getX(), currentPixel.getY(), newRGB);
            }
        }
    }

    private RGB getNewPixelRGB(Pixel currentPixel) {
        int red = currentPixel.getRgb().getRed();
        int green = currentPixel.getRgb().getGreen();
        int blue = currentPixel.getRgb().getBlue();

        int average = greyBusiness.calculateRGBSimples(red, green, blue);

        RGB rgb = null;
        for (Group group : groups) {
            if (average >= group.getMinimum() && average <= group.getMaximum()) {
                rgb = group.getColor();
                break;
            }
        }
        return rgb;
    }

    /**
     * M�todo que cria grupos de acordo com o n�mero passado,
     * atrav�s do histograma da imagem.
     * 
     * @param groupNumber
     */
    private void createGroups(int groupNumber) {
        int difference = maximum - minimum;
        int partition = difference / groupNumber;

        for(int i = 0; i < groupNumber; i++) {
            Group group = new Group();
            group.setMinimum(minimum);
            group.setMaximum(minimum + partition);

            minimum += partition + 1;

            group.setColor(getRandomColor());
            groups.add(group);
        }
    }

    /**
     * M�todo que gera uma cor aleat�ria para preenchimento de um grupo. 
     * @return RGB
     */
    private RGB getRandomColor() {
        RGB rgb = new RGB();

        rgb.setRed(new Random().nextInt(256));
        rgb.setGreen(new Random().nextInt(256));
        rgb.setBlue(new Random().nextInt(256));

        return rgb;
    }

    /**
     * M�todo que preenche os valores m�nimo e m�ximo de cada pixel 
     * do histograma(RGB).
     * 
     * @param values
     */
    private void getMinMaxValues(int[] values) {
        for (int i = 0; i < values.length; i++) {
            if (values[i] != 0) {
                if (maximum == null || i > maximum) {
                    maximum = i;
                }

                if (minimum == null || minimum > i) {
                    minimum = i;
                }
            }
        }
    }
}
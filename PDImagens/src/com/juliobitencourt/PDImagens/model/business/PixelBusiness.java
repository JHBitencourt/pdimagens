package com.juliobitencourt.PDImagens.model.business;

import java.util.ArrayList;
import java.util.List;

import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

public class PixelBusiness {

    private PixelCommunicator communicator;

    public PixelBusiness(PixelCommunicator communicator) {
        this.communicator = communicator;
    }

    public List<Pixel> getNeighborPixels(int x, int y, PixelTecnique tecnique) {
        switch (tecnique) {
        case TECNIQUE_3X3:
            return getPixelsThreeByThree(x, y);
        case TECNIQUE_2X2:
            return getPixelsTwoByTwo(x, y);
        case TECNIQUE_2X2_DIAGONAL:
            return getPixelsTwoByTwoDiagonal(x, y);
        }

        return null;
    }

    private List<Pixel> getPixelsThreeByThree(int x, int y) {
        ArrayList<Pixel> pixels = new ArrayList<Pixel>();

        pixels.add(communicator.getPixel(x - 1, y + 1));
        pixels.add(communicator.getPixel(x, y + 1));
        pixels.add(communicator.getPixel(x + 1, y + 1));
        pixels.add(communicator.getPixel(x - 1, y));
        pixels.add(communicator.getPixel(x, y));
        pixels.add(communicator.getPixel(x + 1, y));
        pixels.add(communicator.getPixel(x - 1, y - 1));
        pixels.add(communicator.getPixel(x - 1, y));
        pixels.add(communicator.getPixel(x + 1, y - 1));

        return pixels;
    }

    private List<Pixel> getPixelsTwoByTwo(int x, int y) {
        ArrayList<Pixel> pixels = new ArrayList<Pixel>();

        pixels.add(communicator.getPixel(x-1, y));
        pixels.add(communicator.getPixel(x, y-1));
        pixels.add(communicator.getPixel(x, y));
        pixels.add(communicator.getPixel(x, y+1));
        pixels.add(communicator.getPixel(x+1, y));

        return pixels;
    }

    private List<Pixel> getPixelsTwoByTwoDiagonal(int x, int y) {
        ArrayList<Pixel> pixels = new ArrayList<Pixel>();

        pixels.add(communicator.getPixel(x-1, y-1));
        pixels.add(communicator.getPixel(x-1, y+1));
        pixels.add(communicator.getPixel(x, y));
        pixels.add(communicator.getPixel(x+1, y-1));
        pixels.add(communicator.getPixel(x+1, y+1));

        return pixels;
    }

    public interface PixelCommunicator {
        Pixel getPixel(int x, int y);

        void updatePixel(int x, int y, RGB newRGB);

        int getImageWidth();

        int getImageHeight();
    }

    public enum PixelTecnique {
        TECNIQUE_3X3(9),
        TECNIQUE_2X2(5),
        TECNIQUE_2X2_DIAGONAL(5);

        private int pixelsNumber;

        private PixelTecnique(int pixelsNumber) {
            this.pixelsNumber = pixelsNumber;
        }

        int getPixelsNumber() {
            return this.pixelsNumber;
        }
    }
}
package com.juliobitencourt.PDImagens.model.business;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;

import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;

/**
 * Classe respons�vel por realizar toda a l�gica dos exerc�cios propostos na Prova:
 * - Criar uma imagem com listras cinzas;
 * - Criar uma imagem com a �rea selecionada invertida;
 * - Identificar se a imagem possui uma �rea vazia ou preenchida;
 * 
 * @author Julio Henrique Bitencourt<julio.henrique.b@gmail.com>
 */
public class ProvaBusiness {

    private static final int BLACK_RGB = 0;

    private PixelCommunicator communicator;

    public ProvaBusiness(PixelCommunicator comunicator) {
        this.communicator = comunicator;
    }

    /**
     * M�todo que ir� dividir a imagem em listras com tons de cinza.
     * 
     * @param image
     * @param columnsNumber
     */
    public void addGreyColumns(BufferedImage image, int columnsNumber) {
        Pixel currentPixel = null;
        RGB newRGB = null;

        int partition = communicator.getImageWidth()/columnsNumber;
        int counter = 0;
        boolean paintGrey = true;

        for (int row = 1; row < communicator.getImageWidth() - 1; row++) {
            counter++;

            for (int column = 1; column < communicator.getImageHeight() - 1; column++) {
                currentPixel = communicator.getPixel(row, column);

                if(paintGrey) {
                    newRGB = getGreyPixel(currentPixel);
                    communicator.updatePixel(row, column, newRGB);
                }
            }

            if(counter == partition) {
                counter = 0;
                paintGrey = !paintGrey;
            }
        }
    }

    /**
     * Retorna o pixel em tons de cinza.
     * 
     * @param currentPixel
     * @return
     */
    private RGB getGreyPixel(Pixel currentPixel) {
        int red = currentPixel.getRgb().getRed();
        int green = currentPixel.getRgb().getGreen();
        int blue = currentPixel.getRgb().getBlue();

        int newRGBValue = (red + green + blue) / 3;

        RGB rgb = new RGB();
        rgb.setRed(newRGBValue);
        rgb.setGreen(newRGBValue);
        rgb.setBlue(newRGBValue);
        return rgb;
    }

    /**
     * M�todo respons�vel por girar uma imagem em 180�.
     * 
     * @param coordinates
     */
    public void drawSquareInvertedImage(Coordinates coordinates) {
        ArrayList<ArrayList<Pixel>> pixels = new ArrayList<>(); 

        //Recupera todos os pixels do quadrado
        for (int row = coordinates.xStart; row <= coordinates.xFinish; row++) {
            ArrayList<Pixel> line = new ArrayList<>();
            Pixel currentPixel = null;

            for (int column = coordinates.yStart; column <= coordinates.yFinish; column++) {
                currentPixel = communicator.getPixel(row, column);
                line.add(currentPixel);
            }

            pixels.add(line);
        }

        //Atualiza a imagem invertida
        for (int row = coordinates.xStart; row <= coordinates.xFinish; row++) {
            ArrayList<Pixel> linePixels = getReversePixels(pixels.get(0));
            pixels.remove(0);

            Pixel currentPixel = null;
            RGB newRGB = null;

            int position = 0;
            for (int column = coordinates.yStart; column <= coordinates.yFinish; column++) {
                currentPixel = linePixels.get(position);
                newRGB = currentPixel.getRgb();
                communicator.updatePixel(row, column, newRGB);

                position++;
            }

            pixels.add(linePixels);
        }
    }

    /**
     * M�todo respons�vel por reverter a ordem que os pixels se encontram no ArrayList.
     * 
     * @param pixels
     * @return
     */
    private ArrayList<Pixel> getReversePixels(ArrayList<Pixel> pixels) {
        Collections.reverse(pixels);
        return pixels;
    }

    /**
     * M�todo que ir� retornar se a imagem cont�m uma �rea retangular vazia
     * ou preenchida.
     * 
     * @return
     */
    public boolean isAnEmptyArea() {
        Pixel currentPixel = null;

        for (int row = 1; row < communicator.getImageWidth() - 1; row++) {
            for (int column = 1; column < communicator.getImageHeight() - 1; column++) {
                currentPixel = communicator.getPixel(row, column);
                boolean isBlack = isABlackPixel(currentPixel);

                if(isBlack) {
                    //O p�xel � o primeiro do canto superior esquerdo.
                    Pixel neighborPixel = getNeighborPixel(row, column);
                    return !isABlackPixel(neighborPixel);
                }
            }
        }

        return true;
    }

    /**
     * M�todo que determina se o pixel � preto ou n�o.
     * 
     * @param currentPixel
     * @return
     */
    private boolean isABlackPixel(Pixel currentPixel) {
        int red = currentPixel.getRgb().getRed();
        int green = currentPixel.getRgb().getGreen();
        int blue = currentPixel.getRgb().getBlue();

        return red == BLACK_RGB && green == BLACK_RGB && blue == BLACK_RGB;
    }

    /**
     * M�todo que ir� retornar o pixel vizinho inferior direito da coordenada
     * passada por par�metro.
     * 
     * @param x
     * @param y
     * @param tecnique
     * @return
     */
    public Pixel getNeighborPixel(int x, int y) {
        return communicator.getPixel(x + 1, y + 1);
    }

    /**
     * Inner Class que representa as coordenadas selecionadas na imagem.
     * @author Julio Henrique Bitencourt<julio.henrique.b@gmail.com>
     *
     */
    public static class Coordinates {
        int xStart;
        int xFinish;
        int yStart;
        int yFinish;

        public void setxStart(int xStart) {
            this.xStart = xStart;
        }

        public void setxFinish(int xFinish) {
            this.xFinish = xFinish;
        }

        public void setyStart(int yStart) {
            this.yStart = yStart;
        }

        public void setyFinish(int yFinish) {
            this.yFinish = yFinish;
        }
    }
}
package com.juliobitencourt.PDImagens.model.business;

import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelCommunicator;
import com.juliobitencourt.PDImagens.model.entities.Histograma;
import com.juliobitencourt.PDImagens.model.entities.Pixel;
import com.juliobitencourt.PDImagens.model.entities.RGB;
import com.juliobitencourt.PDImagens.model.entities.RGB.Layer;

public class HistogramaBusiness {

    private PixelCommunicator communicator;
    private Histograma histogram;
    private Histograma accumulatedHistogram;

    public HistogramaBusiness(PixelCommunicator communicator) {
        this.communicator = communicator;
        this.histogram = new Histograma();
        this.accumulatedHistogram = new Histograma();
    }

    public Histograma extractHistograma() {
        Pixel currentPixel = null;

        for (int row = 1; row < communicator.getImageWidth() - 1; row++) {
            for (int column = 1; column < communicator.getImageHeight() - 1; column++) {
                currentPixel = communicator.getPixel(row, column);
                saveHistogram(currentPixel);
            }
        }

        return histogram;
    }

    public void equalizateHistogram() {
        if (histogram == null) {
            equalizateHistogram();
        }

        accumulatedHistogram.setValuesR(calculateAcumulatedHistogram(Layer.RED));
        accumulatedHistogram.setValuesG(calculateAcumulatedHistogram(Layer.GREEN));
        accumulatedHistogram.setValuesB(calculateAcumulatedHistogram(Layer.BLUE));

        applyNewEqualizatedValues();
    }

    private void applyNewEqualizatedValues() {
        Pixel currentPixel = null;
        RGB newRGB = null;

        for (int row = 1; row < communicator.getImageWidth() - 1; row++) {
            for (int column = 1; column < communicator.getImageHeight() - 1; column++) {
                currentPixel = communicator.getPixel(row, column);
                newRGB = getNewPixelEqualizatedRGB(currentPixel);
                communicator.updatePixel(row, column, newRGB);
            }
        }
    }

    private RGB getNewPixelEqualizatedRGB(Pixel currentPixel) {
        int red = currentPixel.getRgb().getRed();
        int green = currentPixel.getRgb().getGreen();
        int blue = currentPixel.getRgb().getBlue();

        RGB rgb = new RGB();
        rgb.setRed(applyFormula(accumulatedHistogram.getValuesR()[red]));
        rgb.setGreen(applyFormula(accumulatedHistogram.getValuesR()[green]));
        rgb.setBlue(applyFormula(accumulatedHistogram.getValuesR()[blue]));
        return rgb;
    }

    private int applyFormula(int accumulatedValue) {
        double area = (double)communicator.getImageHeight() * communicator.getImageWidth();

        return (int) Math.round((255 / area) * accumulatedValue);
    }

    private int[] calculateAcumulatedHistogram(Layer layer) {
        int[] accumulatedHistogram = new int[Histograma.MAX_PIXELS];
        int[] currentValues = getValuesBasedOnLayer(layer);

        int index = 0;
        for (int value : currentValues) {
            if(index == 0) {
                accumulatedHistogram[index] = value;
            } else {
                accumulatedHistogram[index] = accumulatedHistogram[index-1] + value;
            }

            index++;
        }

        return accumulatedHistogram;
    }

    private int[] getValuesBasedOnLayer(Layer layer) {
        switch (layer) {
        case RED:
            return histogram.getValuesR();
        case GREEN:
            return histogram.getValuesG();
        case BLUE:
            return histogram.getValuesB();
        }
        return null;
    }

    private void saveHistogram(Pixel currentPixel) {
        int red = currentPixel.getRgb().getRed();
        int green = currentPixel.getRgb().getGreen();
        int blue = currentPixel.getRgb().getBlue();

        histogram.getValuesR()[red]++;
        histogram.getValuesG()[green]++;
        histogram.getValuesB()[blue]++;
    }

}
package com.juliobitencourt.PDImagens.model.entities;

public class Histograma {

    public static final int MAX_PIXELS = 256;

    private int[] valuesR = new int[MAX_PIXELS];
    private int[] valuesG = new int[MAX_PIXELS];
    private int[] valuesB = new int[MAX_PIXELS];

    public int[] getValuesR() {
        return valuesR;
    }

    public void setValuesR(int[] valuesR) {
        this.valuesR = valuesR;
    }

    public int[] getValuesG() {
        return valuesG;
    }

    public void setValuesG(int[] valuesG) {
        this.valuesG = valuesG;
    }

    public int[] getValuesB() {
        return valuesB;
    }

    public void setValuesB(int[] valuesB) {
        this.valuesB = valuesB;
    }
}
package com.juliobitencourt.PDImagens.view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.jfree.data.category.DefaultCategoryDataset;

import com.juliobitencourt.PDImagens.controller.AddSubtractController;
import com.juliobitencourt.PDImagens.controller.BlackWhiteController;
import com.juliobitencourt.PDImagens.controller.DrawingController;
import com.juliobitencourt.PDImagens.controller.GreyShadesController;
import com.juliobitencourt.PDImagens.controller.HistogramaController;
import com.juliobitencourt.PDImagens.controller.NegativaController;
import com.juliobitencourt.PDImagens.controller.NoiseController;
import com.juliobitencourt.PDImagens.controller.ProvaController;
import com.juliobitencourt.PDImagens.controller.SegmentationController;
import com.juliobitencourt.PDImagens.model.business.AddSubtractBusiness.Action;
import com.juliobitencourt.PDImagens.model.business.GreyShadesBusiness.GreyShadesTecnique;
import com.juliobitencourt.PDImagens.model.business.NoiseBusiness.NoiseTecnique;
import com.juliobitencourt.PDImagens.model.business.PixelBusiness.PixelTecnique;
import com.juliobitencourt.PDImagens.model.entities.Histograma;

public class PdiView extends Shell {

    public final Color RED = SWTResourceManager.getColor(SWT.COLOR_RED);
    public final Color GREEN = SWTResourceManager.getColor(SWT.COLOR_GREEN);
    public final Color BLUE = SWTResourceManager.getColor(SWT.COLOR_BLUE);

    Composite compositeCoordinator;
    private Table table;

    private CLabel firstImageLabel;
    private CLabel secondImageLabel;
    private CLabel thirdImageLabel;

    private Label coordinatorLabel;
    private Label labelR;
    private Label labelG;
    private Label labelB;

    private FileDialog fileChooser;
    private Image firstImage;
    private Image secondImage;
    private Image thirdImage;
    private String firstImagePath;
    private String secondImagePath;

    // Ru�dos
    private static final String MEDIAN_3x3_PATH = "imgs/mediana_3x3.jpg";
    private static final String MEDIAN_2x2_PATH = "imgs/mediana_2x2.jpg";
    private static final String MEDIAN_2x2_DIAGONAL_PATH = "imgs/mediana_2x2_diagonal.jpg";
    private static final String AVERAGE_3x3_PATH = "imgs/average_3x3.jpg";
    private static final String AVERAGE_2X2_PATH = "imgs/average_2x2.jpg";
    private static final String AVERAGE_2X2_DIAGONAL_PATH = "imgs/average_2x2_diagonal.jpg";

    private NoiseController noiseController = new NoiseController();

    // Tons de Cinza
    private Text textPorcentagemR;
    private Text textPorcentagemG;
    private Text textPorcentagemB;

    private GreyShadesController greyShadesController = new GreyShadesController();

    // Lineariliza��o
    Slider slider;

    private BlackWhiteController blackWhiteController = new BlackWhiteController();

    // Negativa
    private NegativaController negativaController = new NegativaController();

    // Histograma
    private static final String EQUALIZATED_HISTOGRAM_PATH = "imgs/equalizated_histogram.jpg";

    private HistogramaController histogramaController = new HistogramaController();
    private Text textPercentImageOne;

    //Adicionar / Subtrair
    private AddSubtractController addSubtractController = new AddSubtractController();

    //Desenhando um quadrado
    private int[] startCoordinates;
    private int[] endCoordinates;

    private static final String DRAWING_PATH = "imgs/drawing.jpg";
    private DrawingController drawingController = new DrawingController();
    private BufferedImage drawingImage;

    private static final String ADD_PATH = "imgs/add.jpg";
    private static final String SUBTRACT_PATH = "imgs/subtract.jpg";
    private Text textColumnsNumber;

    //Prova
    private ProvaController provaController = new ProvaController();
    private BufferedImage invertedImage;
    Label labelEmptyAnswer;

    private static final String GREY_COLUMNS_PATH = "imgs/grey_columns.jpg";
    private static final String INVERTED_IMAGE_PATH = "imgs/drawing.jpg";
    private Text textGroupNumber;

    private SegmentationController segmentationController = new SegmentationController();
    private static final String SEGMENTADED_IMAGE_PATH = "imgs/segment.jpg";

    /**
     * Launch the application.
     * 
     * @param args
     */
    public static void main(String args[]) {
        try {
            Display display = Display.getDefault();
            PdiView shell = new PdiView(display);
            shell.setMaximized(true);
            shell.open();
            shell.layout();
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the shell.
     * 
     * @param display
     */
    public PdiView(Display display) {
        super(display, SWT.SHELL_TRIM);
        setBackground(SWTResourceManager.getColor(220, 220, 220));
        setLayout(null);

        CTabFolder tabFolder = new CTabFolder(this, SWT.BORDER);
        tabFolder.setBounds(0, 0, 668, 142);
        tabFolder.setSelectionBackground(
                Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));

        CTabItem tabItemRuido = new CTabItem(tabFolder, SWT.NONE);
        tabItemRuido.setText("Ru�do");

        Composite compositeRuido = new Composite(tabFolder, SWT.NONE);
        tabItemRuido.setControl(compositeRuido);

        Button btnMedianThreeByThree = new Button(compositeRuido, SWT.NONE);
        btnMedianThreeByThree.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                try {
                    BufferedImage image = noiseController.removeNoise(ImageIO.read(new File(firstImagePath)),
                            PixelTecnique.TECNIQUE_3X3, NoiseTecnique.MEDIAN);
                    ImageIO.write(image, "png", new File(MEDIAN_3x3_PATH));
                    thirdImage = new Image(null, MEDIAN_3x3_PATH);

                    openImage(thirdImageLabel, MEDIAN_3x3_PATH, thirdImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        btnMedianThreeByThree.setBounds(21, 10, 75, 25);
        btnMedianThreeByThree.setText("Mediana 3x3");

        Button btnMedianTwoByTwo = new Button(compositeRuido, SWT.NONE);
        btnMedianTwoByTwo.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage image = noiseController.removeNoise(ImageIO.read(new File(firstImagePath)),
                            PixelTecnique.TECNIQUE_2X2, NoiseTecnique.MEDIAN);
                    ImageIO.write(image, "jpg", new File(MEDIAN_2x2_PATH));
                    thirdImage = new Image(null, MEDIAN_2x2_PATH);

                    openImage(thirdImageLabel, MEDIAN_2x2_PATH, thirdImage);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnMedianTwoByTwo.setBounds(21, 41, 75, 25);
        btnMedianTwoByTwo.setText("Mediana 2x2");

        Button btnMedianTwoByTwoDiagonal = new Button(compositeRuido, SWT.NONE);
        btnMedianTwoByTwoDiagonal.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage image = noiseController.removeNoise(ImageIO.read(new File(firstImagePath)),
                            PixelTecnique.TECNIQUE_2X2_DIAGONAL, NoiseTecnique.MEDIAN);
                    ImageIO.write(image, "jpg", new File(MEDIAN_2x2_DIAGONAL_PATH));
                    thirdImage = new Image(null, MEDIAN_2x2_DIAGONAL_PATH);

                    openImage(thirdImageLabel, MEDIAN_2x2_DIAGONAL_PATH, thirdImage);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnMedianTwoByTwoDiagonal.setBounds(21, 82, 121, 25);
        btnMedianTwoByTwoDiagonal.setText("Mediana 2x2 Diagonal");

        Button btnAverageThreeByThree = new Button(compositeRuido, SWT.NONE);
        btnAverageThreeByThree.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage image = noiseController.removeNoise(ImageIO.read(new File(firstImagePath)),
                            PixelTecnique.TECNIQUE_3X3, NoiseTecnique.AVERAGE);
                    ImageIO.write(image, "jpg", new File(AVERAGE_3x3_PATH));
                    thirdImage = new Image(null, AVERAGE_3x3_PATH);

                    openImage(thirdImageLabel, AVERAGE_3x3_PATH, thirdImage);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnAverageThreeByThree.setBounds(164, 10, 75, 25);
        btnAverageThreeByThree.setText("M\u00E9dia 3x3");

        Button btnAverageTwoByTwo = new Button(compositeRuido, SWT.NONE);
        btnAverageTwoByTwo.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage image = noiseController.removeNoise(ImageIO.read(new File(firstImagePath)),
                            PixelTecnique.TECNIQUE_2X2, NoiseTecnique.AVERAGE);
                    ImageIO.write(image, "jpg", new File(AVERAGE_2X2_PATH));
                    thirdImage = new Image(null, AVERAGE_2X2_PATH);

                    openImage(thirdImageLabel, AVERAGE_2X2_PATH, thirdImage);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnAverageTwoByTwo.setBounds(164, 41, 75, 25);
        btnAverageTwoByTwo.setText("M\u00E9dia 2x2");

        Button btnAverageTwoByTwoDiagonal = new Button(compositeRuido, SWT.NONE);
        btnAverageTwoByTwoDiagonal.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage image = noiseController.removeNoise(ImageIO.read(new File(firstImagePath)),
                            PixelTecnique.TECNIQUE_2X2_DIAGONAL, NoiseTecnique.AVERAGE);
                    ImageIO.write(image, "jpg", new File(AVERAGE_2X2_DIAGONAL_PATH));
                    thirdImage = new Image(null, AVERAGE_2X2_DIAGONAL_PATH);

                    openImage(thirdImageLabel, AVERAGE_2X2_DIAGONAL_PATH, thirdImage);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnAverageTwoByTwoDiagonal.setBounds(164, 82, 115, 25);
        btnAverageTwoByTwoDiagonal.setText("M\u00E9dia 2x2 Diagonal");

        CTabItem tabItemTonsCinza = new CTabItem(tabFolder, SWT.NONE);
        tabItemTonsCinza.setText("Tons de Cinza");

        Composite compositeTonsCinza = new Composite(tabFolder, SWT.NONE);
        tabItemTonsCinza.setControl(compositeTonsCinza);

        Button buttonTonsCinzaSimples = new Button(compositeTonsCinza, SWT.NONE);
        buttonTonsCinzaSimples.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage image = greyShadesController.turnIntoGrey(ImageIO.read(new File(firstImagePath)),
                            GreyShadesTecnique.SIMPLES);
                    ImageIO.write(image, "jpg", new File(MEDIAN_3x3_PATH));
                    thirdImage = new Image(null, MEDIAN_3x3_PATH);

                    openImage(thirdImageLabel, MEDIAN_3x3_PATH, thirdImage);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        buttonTonsCinzaSimples.setBounds(10, 10, 75, 25);
        buttonTonsCinzaSimples.setText("Simples");

        Button buttonTonsCinzaPonderada = new Button(compositeTonsCinza, SWT.NONE);
        buttonTonsCinzaPonderada.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage image = greyShadesController.turnIntoGrey(ImageIO.read(new File(firstImagePath)),
                            GreyShadesTecnique.PONDERADA, getRGBSelectedPercents());
                    ImageIO.write(image, "jpg", new File(MEDIAN_3x3_PATH));
                    thirdImage = new Image(null, MEDIAN_3x3_PATH);

                    openImage(thirdImageLabel, MEDIAN_3x3_PATH, thirdImage);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        buttonTonsCinzaPonderada.setText("Ponderada");
        buttonTonsCinzaPonderada.setBounds(300, 10, 75, 25);

        textPorcentagemR = new Text(compositeTonsCinza, SWT.BORDER);
        textPorcentagemR.setBounds(385, 12, 41, 21);

        textPorcentagemG = new Text(compositeTonsCinza, SWT.BORDER);
        textPorcentagemG.setBounds(385, 39, 41, 21);

        textPorcentagemB = new Text(compositeTonsCinza, SWT.BORDER);
        textPorcentagemB.setBounds(385, 65, 41, 21);

        Label labelPorcentagemR = new Label(compositeTonsCinza, SWT.NONE);
        labelPorcentagemR.setBounds(432, 15, 23, 15);
        labelPorcentagemR.setText("%R");

        Label labelPorcentagemG = new Label(compositeTonsCinza, SWT.NONE);
        labelPorcentagemG.setText("%G");
        labelPorcentagemG.setBounds(432, 42, 23, 15);

        Label labelPorcentagemB = new Label(compositeTonsCinza, SWT.NONE);
        labelPorcentagemB.setText("%B");
        labelPorcentagemB.setBounds(432, 68, 23, 15);

        CTabItem tabItemLimiarizazao = new CTabItem(tabFolder, SWT.NONE);
        tabItemLimiarizazao.setText("Limiariza��o");

        Composite compositeLimiarizacao = new Composite(tabFolder, SWT.NONE);
        tabItemLimiarizazao.setControl(compositeLimiarizacao);

        slider = new Slider(compositeLimiarizacao, SWT.NONE);
        slider.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage image = blackWhiteController.applyFilter(ImageIO.read(new File(firstImagePath)),
                            slider.getSelection());
                    ImageIO.write(image, "png", new File(MEDIAN_3x3_PATH));
                    thirdImage = new Image(null, MEDIAN_3x3_PATH);

                    openImage(thirdImageLabel, MEDIAN_3x3_PATH, thirdImage);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        slider.setMaximum(255);
        slider.setBounds(10, 74, 534, 17);

        CTabItem tabItemNegativa = new CTabItem(tabFolder, SWT.NONE);
        tabItemNegativa.setText("Negativa");

        Composite compositeNegativa = new Composite(tabFolder, SWT.NONE);
        tabItemNegativa.setControl(compositeNegativa);

        Button btnNegativa = new Button(compositeNegativa, SWT.NONE);
        btnNegativa.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage image = negativaController.applyFilter(ImageIO.read(new File(firstImagePath)));
                    ImageIO.write(image, "png", new File(MEDIAN_3x3_PATH));
                    thirdImage = new Image(null, MEDIAN_3x3_PATH);

                    openImage(thirdImageLabel, MEDIAN_3x3_PATH, thirdImage);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnNegativa.setBounds(10, 27, 75, 25);
        btnNegativa.setText("Negativa");

        CTabItem tabItemHistograma = new CTabItem(tabFolder, SWT.NONE);
        tabItemHistograma.setText("Histograma");

        Composite compositeHistograma = new Composite(tabFolder, SWT.NONE);
        tabItemHistograma.setControl(compositeHistograma);

        Button btnGerarHistograma = new Button(compositeHistograma, SWT.NONE);
        btnGerarHistograma.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    histogramaController.extractHistograma(ImageIO.read(new File(firstImagePath)));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnGerarHistograma.setBounds(22, 29, 112, 25);
        btnGerarHistograma.setText("Gerar Histograma");

        Button button = new Button(compositeHistograma, SWT.NONE);
        button.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage image = histogramaController.equalizateHistograma(ImageIO.read(new File(firstImagePath)));
                    ImageIO.write(image, "png", new File(EQUALIZATED_HISTOGRAM_PATH));
                    thirdImage = new Image(null, EQUALIZATED_HISTOGRAM_PATH);
                    openImage(thirdImageLabel, EQUALIZATED_HISTOGRAM_PATH, thirdImage);

                    Histograma equalizatedHistogram = histogramaController.extractHistograma(image);

                    DefaultCategoryDataset ds = new DefaultCategoryDataset();
                    for(int i = 0; i<256; i++) {
                        ds.addValue(equalizatedHistogram.getValuesR()[i], "", i+"");
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        button.setText("Equalizar Histograma");
        button.setBounds(193, 29, 124, 25);

        CTabItem tabItemAdicaoSubtracao = new CTabItem(tabFolder, SWT.NONE);
        tabItemAdicaoSubtracao.setText("Adi��o/Subtra��o");

        Composite compositeAdicao = new Composite(tabFolder, SWT.NONE);
        tabItemAdicaoSubtracao.setControl(compositeAdicao);

        Label lblImg = new Label(compositeAdicao, SWT.NONE);
        lblImg.setBounds(10, 31, 55, 15);
        lblImg.setText("Img 1");

        textPercentImageOne = new Text(compositeAdicao, SWT.BORDER);
        textPercentImageOne.setBounds(10, 56, 55, 21);

        Button btnAdicionar = new Button(compositeAdicao, SWT.NONE);
        btnAdicionar.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage firstImage = ImageIO.read(new File(firstImagePath));
                    BufferedImage secondImage = ImageIO.read(new File(secondImagePath));

                    int fator = Integer.parseInt(textPercentImageOne.getText());
                    BufferedImage image = addSubtractController.execute(Action.ADD, fator, firstImage, secondImage);

                    ImageIO.write(image, "png", new File(ADD_PATH));
                    thirdImage = new Image(null, ADD_PATH);

                    openImage(thirdImageLabel, ADD_PATH, thirdImage);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnAdicionar.setBounds(198, 26, 75, 25);
        btnAdicionar.setText("Adicionar");

        Button btnSubtrair = new Button(compositeAdicao, SWT.NONE);
        btnSubtrair.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    BufferedImage firstImage = ImageIO.read(new File(firstImagePath));
                    BufferedImage secondImage = ImageIO.read(new File(secondImagePath));

                    int fator = Integer.parseInt(textPercentImageOne.getText());
                    BufferedImage image = addSubtractController.execute(Action.SUBTRACT, fator, firstImage, secondImage);

                    ImageIO.write(image, "png", new File(ADD_PATH));
                    thirdImage = new Image(null, ADD_PATH);

                    openImage(thirdImageLabel, ADD_PATH, thirdImage);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        btnSubtrair.setText("Subtrair");
        btnSubtrair.setBounds(198, 54, 75, 25);
        
        CTabItem tbtmInversImage = new CTabItem(tabFolder, SWT.NONE);
        tbtmInversImage.setText("Inverse Image");
        
        Composite composite = new Composite(tabFolder, SWT.NONE);
        tbtmInversImage.setControl(composite);
        
        Button btnInverseImage = new Button(composite, SWT.NONE);
        btnInverseImage.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                
                
            }
        });
        btnInverseImage.setBounds(10, 21, 93, 25);
        btnInverseImage.setText("Inverse Image");
        
        CTabItem tabItem = new CTabItem(tabFolder, SWT.NONE);
        tabItem.setText("Prova");
        
        Composite composite_1 = new Composite(tabFolder, SWT.NONE);
        tabItem.setControl(composite_1);
        
        textColumnsNumber = new Text(composite_1, SWT.BORDER);
        textColumnsNumber.setBounds(10, 49, 76, 21);
        
        Label lblColunas = new Label(composite_1, SWT.NONE);
        lblColunas.setBounds(10, 30, 55, 15);
        lblColunas.setText("Colunas");
        
        Button btnProcessar = new Button(composite_1, SWT.NONE);
        btnProcessar.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                int columns = Integer.parseInt(textColumnsNumber.getText());
                //jhb
                try {
                    BufferedImage image = provaController.addGreyColumns(ImageIO.read(new File(firstImagePath)), columns);
                    ImageIO.write(image, "png", new File(GREY_COLUMNS_PATH));
                    thirdImage = new Image(null, GREY_COLUMNS_PATH);

                    openImage(thirdImageLabel, GREY_COLUMNS_PATH, thirdImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        btnProcessar.setBounds(11, 76, 75, 25);
        btnProcessar.setText("Processar");
        
        Label label = new Label(composite_1, SWT.SEPARATOR | SWT.VERTICAL);
        label.setBounds(140, 10, 2, 91);
        
        Button btnVazioOuPreenchido = new Button(composite_1, SWT.NONE);
        btnVazioOuPreenchido.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                //JHB
                try {
                    boolean answer = provaController.isAnEmptyArea(ImageIO.read(new File(firstImagePath)));

                    if (answer) {
                        labelEmptyAnswer.setText("Vazio");
                    } else {
                        labelEmptyAnswer.setText("Preenchido");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        btnVazioOuPreenchido.setBounds(168, 47, 159, 25);
        btnVazioOuPreenchido.setText("Vazio ou Preenchido");
        
        labelEmptyAnswer = new Label(composite_1, SWT.NONE);
        labelEmptyAnswer.setBounds(218, 86, 55, 15);
        labelEmptyAnswer.setText("resposta");
        
        CTabItem tabItemSegmentation = new CTabItem(tabFolder, SWT.NONE);
        tabItemSegmentation.setText("Segmenta��o");
        
        Composite compositeSegmentation = new Composite(tabFolder, SWT.NONE);
        tabItemSegmentation.setControl(compositeSegmentation);
        
        textGroupNumber = new Text(compositeSegmentation, SWT.BORDER);
        textGroupNumber.setBounds(10, 41, 76, 21);
        
        Label lblNroGrupos = new Label(compositeSegmentation, SWT.NONE);
        lblNroGrupos.setBounds(10, 24, 76, 15);
        lblNroGrupos.setText("Nro grupos:");
        
        Button btnSegmentar = new Button(compositeSegmentation, SWT.NONE);
        btnSegmentar.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                int groupNumber = Integer.parseInt(textGroupNumber.getText());
                //HERE
                try {
                    segmentationController = new SegmentationController();
                    BufferedImage image = segmentationController.segmentateImage(ImageIO.read(new File(firstImagePath)), groupNumber); 
                    ImageIO.write(image, "png", new File(SEGMENTADED_IMAGE_PATH));
                    thirdImage = new Image(null, SEGMENTADED_IMAGE_PATH);

                    openImage(thirdImageLabel, SEGMENTADED_IMAGE_PATH, thirdImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        btnSegmentar.setBounds(10, 82, 83, 25);
        btnSegmentar.setText("Segmentar");

        ScrolledComposite scrolledComposite = new ScrolledComposite(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
        scrolledComposite.setBounds(10, 256, 292, 418);

        firstImageLabel = new CLabel(scrolledComposite, SWT.NONE);
        firstImageLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent event) {
                startCoordinates = new int[]{event.x, event.y};
            }
            @Override
            public void mouseUp(MouseEvent event) {
                encontrarPixelParaLista(event.x, event.y, firstImage, "1");

                endCoordinates = new int[]{event.x, event.y};
                try {
                    if(drawingImage != null) {
//                        drawingImage = drawingController.drawGreySquare(drawingImage, startCoordinates, endCoordinates); 
//                        drawingImage = drawingController.drawSquare(drawingImage, startCoordinates, endCoordinates);
                        ImageIO.write(drawingImage, "png", new File(DRAWING_PATH));
                        thirdImage = new Image(null, DRAWING_PATH);

                        openImage(thirdImageLabel, DRAWING_PATH, thirdImage);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        firstImageLabel.addMouseMoveListener(new MouseMoveListener() {
            @Override
            public void mouseMove(MouseEvent event) {
                encontrarPixel(event.x, event.y, firstImage);
            }
        });
        scrolledComposite.setContent(firstImageLabel);
        scrolledComposite.setMinSize(firstImageLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT));

        ScrolledComposite scrolledComposite2 = new ScrolledComposite(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
        scrolledComposite2.setBounds(327, 256, 303, 418);

        secondImageLabel = new CLabel(scrolledComposite2, SWT.NONE);
        secondImageLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent event) {
                startCoordinates = new int[]{event.x, event.y};
            }
            @Override
            public void mouseUp(MouseEvent event) {
                encontrarPixelParaLista(event.x, event.y, secondImage, "2");

                endCoordinates = new int[]{event.x, event.y};
                try {
                    if(invertedImage != null) {
                        //JHB
                        invertedImage = provaController.drawSquareInvertedImage(invertedImage, startCoordinates, endCoordinates);
                        ImageIO.write(invertedImage, "png", new File(INVERTED_IMAGE_PATH));
                        thirdImage = new Image(null, INVERTED_IMAGE_PATH);

                        openImage(thirdImageLabel, INVERTED_IMAGE_PATH, thirdImage);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        secondImageLabel.addMouseMoveListener(new MouseMoveListener() {
            public void mouseMove(MouseEvent event) {
                encontrarPixel(event.x, event.y, secondImage);
            }
        });
        scrolledComposite2.setContent(secondImageLabel);
        scrolledComposite2.setMinSize(secondImageLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT));
        scrolledComposite2.setMinSize(new Point(61, 21));

        ScrolledComposite scrolledComposite3 = new ScrolledComposite(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
        scrolledComposite3.setBounds(653, 256, 287, 418);

        thirdImageLabel = new CLabel(scrolledComposite3, SWT.NONE);
        scrolledComposite3.setContent(thirdImageLabel);
        scrolledComposite3.setMinSize(thirdImageLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT));
        scrolledComposite3.setMinSize(new Point(61, 21));

        Button buttonFirstImage = new Button(this, SWT.NONE);
        buttonFirstImage.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                firstImagePath = fileChooser.open();
                firstImage = new Image(null, firstImagePath);

                openImage(firstImageLabel, firstImagePath, firstImage);

                try {
                    drawingImage = ImageIO.read(new File(firstImagePath));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        buttonFirstImage.setBounds(10, 212, 75, 25);
        buttonFirstImage.setText("Imagem 1");

        Button buttonSecondImage = new Button(this, SWT.NONE);
        buttonSecondImage.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent arg0) {
                secondImagePath = fileChooser.open();
                secondImage = new Image(null, secondImagePath);

                openImage(secondImageLabel, secondImagePath, secondImage);

                try {
                    invertedImage = ImageIO.read(new File(secondImagePath));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        buttonSecondImage.setText("Imagem 2");
        buttonSecondImage.setBounds(327, 212, 75, 25);

        compositeCoordinator = new Composite(this, SWT.NONE);
        compositeCoordinator.setBounds(485, 153, 75, 71);

        coordinatorLabel = new Label(compositeCoordinator, SWT.NONE);
        coordinatorLabel.setBounds(10, 32, 55, 15);

        labelR = new Label(this, SWT.NONE);
        labelR.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
        labelR.setFont(SWTResourceManager.getFont("Segoe UI", 15, SWT.NORMAL));
        labelR.setBounds(10, 164, 61, 34);
        labelR.setText("labelR");

        labelG = new Label(this, SWT.NONE);
        labelG.setForeground(SWTResourceManager.getColor(SWT.COLOR_GREEN));
        labelG.setFont(SWTResourceManager.getFont("Segoe UI", 15, SWT.NORMAL));
        labelG.setBounds(110, 164, 61, 26);
        labelG.setText("labelG");

        labelB = new Label(this, SWT.NONE);
        labelB.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLUE));
        labelB.setFont(SWTResourceManager.getFont("Segoe UI", 15, SWT.NORMAL));
        labelB.setText("labelB");
        labelB.setBounds(216, 164, 61, 26);

        table = new Table(this, SWT.BORDER | SWT.FULL_SELECTION);
        table.setBounds(674, 0, 256, 250);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        TableColumn tblclmnR = new TableColumn(table, SWT.NONE);
        tblclmnR.setWidth(49);
        tblclmnR.setText("R");

        TableColumn tblclmnG = new TableColumn(table, SWT.NONE);
        tblclmnG.setWidth(48);
        tblclmnG.setText("G");

        TableColumn tblclmnB = new TableColumn(table, SWT.NONE);
        tblclmnB.setWidth(46);
        tblclmnB.setText("B");

        TableColumn tblclmnX = new TableColumn(table, SWT.NONE);
        tblclmnX.setWidth(47);
        tblclmnX.setText("X");

        TableColumn tblclmnY = new TableColumn(table, SWT.NONE);
        tblclmnY.setWidth(44);
        tblclmnY.setText("Y");

        TableColumn tblclmnImagem = new TableColumn(table, SWT.NONE);
        tblclmnImagem.setWidth(36);
        tblclmnImagem.setText("Img");

        TableColumn tblclmnCor = new TableColumn(table, SWT.NONE);
        tblclmnCor.setWidth(80);
        tblclmnCor.setText("Cor");
        createContents();
        configureFileChooser();
    }

    private void configureFileChooser() {
        fileChooser = new FileDialog(this, SWT.OPEN);
        fileChooser.setText("Abrir");
        fileChooser.setFilterPath("imagens");
        String[] filterText = { "*.*", ".png", ".bmp", ".jpg", ".jpeg" };
        fileChooser.setFilterExtensions(filterText);
    }

    private void openImage(CLabel label, String path, Image image) {
        if (path != null && !path.isEmpty()) {
            image = new Image(null, path);
            label.setBackground(image);
            label.setBounds(label.getBounds().x, label.getBounds().y, image.getBounds().width,
                    image.getBounds().height);
        }
    }

    private void encontrarPixel(int x, int y, Image image) {
        coordinatorLabel.setText(x + " - " + y);
        ImageData imageData = image.getImageData();
        PaletteData paletData = imageData.palette;

        if (paletData != null && x > -1 && y > -1) {
            int pixel = imageData.getPixel(x, y);
            RGB rgb = paletData.getRGB(pixel);
            labelR.setText("R: " + rgb.red);
            labelG.setText("G: " + rgb.green);
            labelB.setText("B: " + rgb.blue);
            compositeCoordinator.setBackground(new Color(null, rgb));
        }
    }

    private void encontrarPixelParaLista(int x, int y, Image image, String indexImagem) {
        if (image == null)
            return;

        TableItem tableItem = new TableItem(table, SWT.NONE);
        ImageData imageData = image.getImageData();
        PaletteData paletData = imageData.palette;

        if (paletData != null && x > -1 && y > -1) {
            tableItem.setForeground(0, this.RED);
            tableItem.setForeground(1, this.GREEN);
            tableItem.setForeground(2, this.BLUE);

            int pixel = imageData.getPixel(x, y);
            RGB rgb = paletData.getRGB(pixel);
            tableItem.setText(0, String.valueOf(rgb.red));
            tableItem.setText(1, String.valueOf(rgb.green));
            tableItem.setText(2, String.valueOf(rgb.blue));
            tableItem.setText(3, String.valueOf(x));
            tableItem.setText(4, String.valueOf(y));
            tableItem.setText(5, indexImagem);
            Color color = new Color(null, rgb);
            tableItem.setBackground(6, color);
        }
    }

    private int[] getRGBSelectedPercents() {
        int[] rgbPercents = new int[3];
        rgbPercents[0] = Integer.valueOf(textPorcentagemR.getText());
        rgbPercents[1] = Integer.valueOf(textPorcentagemG.getText());
        rgbPercents[2] = Integer.valueOf(textPorcentagemB.getText());
        return rgbPercents;
    }

    /**
     * Create contents of the shell.
     */
    protected void createContents() {
        setText("PDI - Processamento Digital de Imagens");
        setSize(956, 722);

    }

    @Override
    protected void checkSubclass() {
    }
}